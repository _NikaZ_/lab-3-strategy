using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Emmit : IStrategy
{
    private int _emmitCount;

    public Emmit(int emmitCount)
    {
        _emmitCount = emmitCount;
    }

    public void Perform(Transform transform)
    {
        ParticleSystem particleSystem = transform.GetComponentInChildren<ParticleSystem>();
        particleSystem.Emit(_emmitCount);
    }
}
